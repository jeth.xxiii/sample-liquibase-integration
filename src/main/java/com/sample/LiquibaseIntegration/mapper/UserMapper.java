package com.sample.LiquibaseIntegration.mapper;

import com.sample.LiquibaseIntegration.Entity.User;

public interface UserMapper {

	User getUserByUsername(String username);

}
