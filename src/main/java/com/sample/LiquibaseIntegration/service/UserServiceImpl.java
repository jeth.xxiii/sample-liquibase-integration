package com.sample.LiquibaseIntegration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sample.LiquibaseIntegration.Entity.User;
import com.sample.LiquibaseIntegration.mapper.UserMapper;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public User getUserByUsername(String username) {
		return userMapper.getUserByUsername(username);
	}

}
